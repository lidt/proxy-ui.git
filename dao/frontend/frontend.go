package frontend

import (
	"time"
	"gitee.com/lidt/proxy-ui/mysql"
)

type Frontend struct {
	Id int
	Name string
	Bind_port int
	Mode string
	Backend string
	Create_time time.Time
	Update_time time.Time
}

func GetAll() []Frontend {
	var frontends []Frontend
	rows, err := mysql.DB.Query("select * from frontend")

	if err != nil {
		return nil
	}

	for rows.Next() {
		var frontend = Frontend{}
		rows.Scan(
			&frontend.Id,
			&frontend.Name,
			&frontend.Bind_port,
			&frontend.Mode,
			&frontend.Backend,
			&frontend.Create_time,
			&frontend.Update_time)

		frontends = append(frontends, frontend)
	}

	return frontends
}
