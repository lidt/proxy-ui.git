package defaults

import (
	"database/sql"
	"gitee.com/lidt/proxy-ui/mysql"
)

type Defaults struct {
	Mode            string
	Retries         int
	Timeout_connect string
	Timeout_client  string
	Timeout_server  string
	Timeout_check   string
}

func GetOne() *Defaults {
	var defaults = &Defaults{}
	err := mysql.DB.QueryRow("select * from defaults limit 1").Scan(defaults.Mode, defaults.Retries,
		defaults.Timeout_connect, defaults.Timeout_client, defaults.Timeout_server, defaults.Timeout_check)
	if err == sql.ErrNoRows {
		return NewDefaults()
	}

	return defaults
}

func NewDefaults() *Defaults {
	return &Defaults{
		Mode: "tcp",
		Retries: 3,
		Timeout_connect: "10s",
		Timeout_client: "60s",
		Timeout_server: "60s",
		Timeout_check: "10s",
	}
}
