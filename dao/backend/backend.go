package backend

import (
	"time"
	"gitee.com/lidt/proxy-ui/mysql"
	"database/sql"
)

type Backend struct {
	Id int
	Name string
	Balance string
	Servers string
	Create_time time.Time
	Update_time time.Time
}

func GetByName(name string) *Backend {
	var backend = &Backend{}

	err := mysql.DB.QueryRow("select * from backend where name = ?", name).Scan(
		&backend.Id,
		&backend.Name,
		&backend.Balance,
		&backend.Servers,
		&backend.Create_time,
		&backend.Update_time,
	)

	if err == sql.ErrNoRows {
		return nil
	}

	return backend
}