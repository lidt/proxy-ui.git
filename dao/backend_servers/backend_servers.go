package backend_servers

import (
	"time"
	"gitee.com/lidt/proxy-ui/mysql"
	"database/sql"
)

type Backend_servers struct {
	Id int
	Name string
	Host string
	Port int
	Maxconn int
	Create_time time.Time
	Update_time time.Time
}

func GetById(id int) *Backend_servers {
	var backend_server = &Backend_servers{}

	err := mysql.DB.QueryRow("select * from backend_servers where id = ?", id).Scan(
		&backend_server.Id,
		&backend_server.Name,
		&backend_server.Host,
		&backend_server.Port,
		&backend_server.Maxconn,
		&backend_server.Create_time,
		&backend_server.Update_time,
	)

	if err == sql.ErrNoRows {
		return nil
	}

	return backend_server
}
