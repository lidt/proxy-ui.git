package global

import (
	"database/sql"
	"gitee.com/lidt/proxy-ui/mysql"
)

type Global struct {
	Chroot string
	Log string
	Maxconn int
	User string
	Group string
	Daemon bool
	Pidfile string
}

func GetOne() *Global {
	var global = &Global{}
	row := mysql.DB.QueryRow("select * from global limit 1")
	err := row.Scan(global.Chroot, global.Log, global.Maxconn,
		global.User, global.Group, global.Daemon, global.Pidfile)
	if err == sql.ErrNoRows {
		return NewDefaultGlobal()
	}

	return global
}
func NewDefaultGlobal() *Global {
	return &Global{
		Chroot: "/var/lib/haproxy",
		Log: "127.0.0.1 local0",
		Maxconn: 4096,
		User: "haproxy",
		Group: "haproxy",
		Daemon: true,
		Pidfile: "/var/run/haproxy.pid",
	}
}
