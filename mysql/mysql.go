package mysql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"gitee.com/lidt/proxy-ui/systemcfg"
	"gitee.com/lidt/proxy-ui/common/utils/logger"
)

var DB *sql.DB

func init() {
	var err error
	dbUrl := systemcfg.MYSQL_USER+":"+systemcfg.MYSQL_PASS+
		"@tcp("+systemcfg.MYSQL_HOST+":"+systemcfg.MYSQL_PORT+")/"+
		systemcfg.MYSQL_DB+"?charset=utf8"
	DB, err = sql.Open("mysql", dbUrl)
	if err != nil {
		log.Fatal("mysql connect error, ", err)
	}
}
