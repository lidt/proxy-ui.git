package utils

import (
	"net"
	"time"
	"fmt"
	"gitee.com/lidt/proxy-ui.git/common/utils/logger"
)

func TestConnTcp(addr string, timeout, interval int) error {
	success := make(chan int)
	cancel := make(chan int)

	go func() {
		for true {
			select {
			case <-cancel:
				break;
			default:
				conn, err := net.DialTimeout("tcp", addr, time.Duration(timeout) * time.Second)
				if err != nil {
					log.Errorf("failed to connect to tcp://%s, retry after %d seconds :%v",
						addr, interval, err)
					time.Sleep(time.Duration(interval) * time.Second)
					continue
				}
				if err = conn.Close(); err != nil {
					log.Errorf("failed to close the connection: %v", err)
				}
				success <- 1
				break
			}
		}
	}()

	select {
	case <-success:
		return nil
	case <-time.After(time.Duration(timeout) * time.Second):
		cancel <- 1
		return fmt.Errorf("failed to connect to tcp:%s after %d seconds", addr, timeout)
	}
}
