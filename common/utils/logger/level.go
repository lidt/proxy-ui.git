package log

type Level int

const (
	DebugLevel Level = iota
	InfoLevel
	WarningLevel
	ErrorLevel
	FatalLevel
)

func (l Level) string() (lvl string) {
	switch l {
	case DebugLevel:
		lvl = "DEBUG"
	case InfoLevel:
		lvl = "INFO"
	case WarningLevel:
		lvl = "WARNING"
	case ErrorLevel:
		lvl = "ERROR"
	case FatalLevel:
		lvl = "FATAL"
	}

	return
}

