package log

import "time"

type Record struct {
	Time time.Time
	Msg string
	Line string
	Lvl Level
}

// NewRecord creates a record according to the arguments provided and returns it
func NewRecord(time time.Time, msg, line string, lvl Level) *Record {
	return &Record{
		Time: time,
		Msg:  msg,
		Line: line,
		Lvl:  lvl,
	}
}