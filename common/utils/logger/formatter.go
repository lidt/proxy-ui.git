package log

type Formatter interface {
	Format (*Record) ([]byte, error)
}
