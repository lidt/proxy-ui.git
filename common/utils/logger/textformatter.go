package log

import (
	"time"
	"fmt"
)

var defaultTimeFormat = time.RFC3339 // 2006-01-02T15:04:05Z07:00

type TextFormatter struct {
	timeFormat string
}

func NewTextFormatter() *TextFormatter {
	return &TextFormatter{
		timeFormat:	defaultTimeFormat,
	}
}

func (t *TextFormatter) Format(r *Record) (b []byte, err error) {
	s := fmt.Sprintf("%s [%s] ", r.Time.Format(t.timeFormat), r.Lvl.string())

	if len(r.Line) != 0 {
		s = s + r.Line + " "
	}

	if len(r.Msg) != 0 {
		s = s + r.Msg
	}

	b = []byte(s)

	if len(b) == 0 || b[len(b)-1] != '\n' {
		b = append(b, '\n')
	}

	return
}

func (t *TextFormatter) SetTimeFormat(fmt string) {
	if len(fmt) != 0 {
		t.timeFormat = fmt
	}
}